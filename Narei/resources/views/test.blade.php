<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/style.css">
    <title>123</title>
</head>

<body>


<div class="big-phone__row-contact">
    <div class="big-phone__row-contact-individuals">

        <ul class="big-phone__list-contact-individuals">
            <li class="big-phone__item-contact-individuals">
                <p class="big-phone__contact-text">
                    @lang('Для физических лиц:')
                </p>
            </li>

            <li class="big-phone__item-contact-individuals">
                <a href="javascript:void(0)" class="big-phone__link-contact"
                   data-bs-toggle="dropdown" aria-expanded="false">
                                        <span class="big-phone__contact-dropdown-link-phone-icon">
                                            <svg width="12" height="9" viewBox="0 0 12 9" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M6 0.25C3.91917 0.25 2.03083 1.0597 0.632917 2.37432C0.550417 2.4562 0.5 2.56992 0.5 2.69729C0.5 2.82466 0.550417 2.93838 0.632917 3.02026L1.76958 4.14838C1.85208 4.23026 1.96667 4.2803 2.10417 4.2803C2.21875 4.2803 2.33333 4.23026 2.41583 4.15293C2.77792 3.81632 3.19042 3.53429 3.635 3.31139C3.78625 3.23861 3.89167 3.08395 3.89167 2.90199V1.49184C4.55625 1.2735 5.26208 1.15977 6 1.15977C6.72875 1.15977 7.43917 1.2735 8.10375 1.48729V2.89744C8.10375 3.07485 8.20917 3.23406 8.36042 3.30684C8.80958 3.52974 9.20833 3.81632 9.58417 4.14838C9.66667 4.23026 9.78125 4.27575 9.89583 4.27575C10.0333 4.27575 10.1479 4.22571 10.2304 4.14383L11.3671 3.01571C11.4496 2.93383 11.5 2.82011 11.5 2.69274C11.5 2.56538 11.445 2.4562 11.3625 2.37432C9.96458 1.0597 8.07625 0.25 6 0.25ZM4.625 2.06955V3.43421C4.625 3.43421 1.875 5.70865 1.875 7.07331V8.89286H10.125V7.07331C10.125 5.70865 7.375 3.43421 7.375 3.43421V2.06955H6.45833V2.97932H5.54167V2.06955H4.625ZM6 4.34398C6.48623 4.34398 6.95255 4.53569 7.29636 4.87692C7.64018 5.21815 7.83333 5.68096 7.83333 6.16353C7.83333 6.64611 7.64018 7.10892 7.29636 7.45015C6.95255 7.79138 6.48623 7.98308 6 7.98308C5.51377 7.98308 5.04745 7.79138 4.70364 7.45015C4.35982 7.10892 4.16667 6.64611 4.16667 6.16353C4.16667 5.68096 4.35982 5.21815 4.70364 4.87692C5.04745 4.53569 5.51377 4.34398 6 4.34398V4.34398ZM6 5.02632C5.69611 5.02632 5.40466 5.14613 5.18977 5.3594C4.97489 5.57267 4.85417 5.86192 4.85417 6.16353C4.85417 6.46514 4.97489 6.7544 5.18977 6.96767C5.40466 7.18094 5.69611 7.30075 6 7.30075C6.30389 7.30075 6.59534 7.18094 6.81023 6.96767C7.02511 6.7544 7.14583 6.46514 7.14583 6.16353C7.14583 5.86192 7.02511 5.57267 6.81023 5.3594C6.59534 5.14613 6.30389 5.02632 6 5.02632V5.02632Z"
                                                        fill="#2C79D6"/>
                                            </svg>
                                        </span>
                    <span
                        class="big-phone__contact-dropdown-link-phone-phone fs-12 fs-lg-10 color-blue">
                                            @lang('common.main_office_phone_label_1')
                                        </span>
                </a>
            </li>

            <li class="big-phone__item-contact-individuals">
                <a href="javascript:void(0)" class="big-phone__big-phone__link-contact"
                   data-bs-toggle="dropdown" aria-expanded="false">
                                        <span class="big-phone__contact-dropdown-link-phone-icon">
                                            <svg width="12" height="9" viewBox="0 0 12 9" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M6 0.25C3.91917 0.25 2.03083 1.0597 0.632917 2.37432C0.550417 2.4562 0.5 2.56992 0.5 2.69729C0.5 2.82466 0.550417 2.93838 0.632917 3.02026L1.76958 4.14838C1.85208 4.23026 1.96667 4.2803 2.10417 4.2803C2.21875 4.2803 2.33333 4.23026 2.41583 4.15293C2.77792 3.81632 3.19042 3.53429 3.635 3.31139C3.78625 3.23861 3.89167 3.08395 3.89167 2.90199V1.49184C4.55625 1.2735 5.26208 1.15977 6 1.15977C6.72875 1.15977 7.43917 1.2735 8.10375 1.48729V2.89744C8.10375 3.07485 8.20917 3.23406 8.36042 3.30684C8.80958 3.52974 9.20833 3.81632 9.58417 4.14838C9.66667 4.23026 9.78125 4.27575 9.89583 4.27575C10.0333 4.27575 10.1479 4.22571 10.2304 4.14383L11.3671 3.01571C11.4496 2.93383 11.5 2.82011 11.5 2.69274C11.5 2.56538 11.445 2.4562 11.3625 2.37432C9.96458 1.0597 8.07625 0.25 6 0.25ZM4.625 2.06955V3.43421C4.625 3.43421 1.875 5.70865 1.875 7.07331V8.89286H10.125V7.07331C10.125 5.70865 7.375 3.43421 7.375 3.43421V2.06955H6.45833V2.97932H5.54167V2.06955H4.625ZM6 4.34398C6.48623 4.34398 6.95255 4.53569 7.29636 4.87692C7.64018 5.21815 7.83333 5.68096 7.83333 6.16353C7.83333 6.64611 7.64018 7.10892 7.29636 7.45015C6.95255 7.79138 6.48623 7.98308 6 7.98308C5.51377 7.98308 5.04745 7.79138 4.70364 7.45015C4.35982 7.10892 4.16667 6.64611 4.16667 6.16353C4.16667 5.68096 4.35982 5.21815 4.70364 4.87692C5.04745 4.53569 5.51377 4.34398 6 4.34398V4.34398ZM6 5.02632C5.69611 5.02632 5.40466 5.14613 5.18977 5.3594C4.97489 5.57267 4.85417 5.86192 4.85417 6.16353C4.85417 6.46514 4.97489 6.7544 5.18977 6.96767C5.40466 7.18094 5.69611 7.30075 6 7.30075C6.30389 7.30075 6.59534 7.18094 6.81023 6.96767C7.02511 6.7544 7.14583 6.46514 7.14583 6.16353C7.14583 5.86192 7.02511 5.57267 6.81023 5.3594C6.59534 5.14613 6.30389 5.02632 6 5.02632V5.02632Z"
                                                        fill="#2C79D6"/>
                                            </svg>
                                        </span>
                    <span
                        class="big-phone__contact-dropdown-link-phone-phone fs-12 fs-lg-10 color-blue">
                                            @lang('common.main_office_phone_label_1')
                                        </span>
                </a>
            </li>
        </ul>

    </div>

    <div class="big-phone__row-contact-entity">

        <ul class="big-phone__list-contact-entity">
            <li class="big-phone__item-contact-entity">
                <p class="big-phone__contact-text">
                    @lang('Для юридических лиц:')
                </p>
            </li>

            <li class="big-phone__item-contact-entity">
                <a href="javascript:void(0)" class="big-phone__big-phone__link-contact"
                   data-bs-toggle="dropdown" aria-expanded="false">
                                        <span class="big-phone__contact-dropdown-link-phone-icon">
                                            <svg width="12" height="9" viewBox="0 0 12 9" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M6 0.25C3.91917 0.25 2.03083 1.0597 0.632917 2.37432C0.550417 2.4562 0.5 2.56992 0.5 2.69729C0.5 2.82466 0.550417 2.93838 0.632917 3.02026L1.76958 4.14838C1.85208 4.23026 1.96667 4.2803 2.10417 4.2803C2.21875 4.2803 2.33333 4.23026 2.41583 4.15293C2.77792 3.81632 3.19042 3.53429 3.635 3.31139C3.78625 3.23861 3.89167 3.08395 3.89167 2.90199V1.49184C4.55625 1.2735 5.26208 1.15977 6 1.15977C6.72875 1.15977 7.43917 1.2735 8.10375 1.48729V2.89744C8.10375 3.07485 8.20917 3.23406 8.36042 3.30684C8.80958 3.52974 9.20833 3.81632 9.58417 4.14838C9.66667 4.23026 9.78125 4.27575 9.89583 4.27575C10.0333 4.27575 10.1479 4.22571 10.2304 4.14383L11.3671 3.01571C11.4496 2.93383 11.5 2.82011 11.5 2.69274C11.5 2.56538 11.445 2.4562 11.3625 2.37432C9.96458 1.0597 8.07625 0.25 6 0.25ZM4.625 2.06955V3.43421C4.625 3.43421 1.875 5.70865 1.875 7.07331V8.89286H10.125V7.07331C10.125 5.70865 7.375 3.43421 7.375 3.43421V2.06955H6.45833V2.97932H5.54167V2.06955H4.625ZM6 4.34398C6.48623 4.34398 6.95255 4.53569 7.29636 4.87692C7.64018 5.21815 7.83333 5.68096 7.83333 6.16353C7.83333 6.64611 7.64018 7.10892 7.29636 7.45015C6.95255 7.79138 6.48623 7.98308 6 7.98308C5.51377 7.98308 5.04745 7.79138 4.70364 7.45015C4.35982 7.10892 4.16667 6.64611 4.16667 6.16353C4.16667 5.68096 4.35982 5.21815 4.70364 4.87692C5.04745 4.53569 5.51377 4.34398 6 4.34398V4.34398ZM6 5.02632C5.69611 5.02632 5.40466 5.14613 5.18977 5.3594C4.97489 5.57267 4.85417 5.86192 4.85417 6.16353C4.85417 6.46514 4.97489 6.7544 5.18977 6.96767C5.40466 7.18094 5.69611 7.30075 6 7.30075C6.30389 7.30075 6.59534 7.18094 6.81023 6.96767C7.02511 6.7544 7.14583 6.46514 7.14583 6.16353C7.14583 5.86192 7.02511 5.57267 6.81023 5.3594C6.59534 5.14613 6.30389 5.02632 6 5.02632V5.02632Z"
                                                        fill="#2C79D6"/>
                                            </svg>
                                        </span>
                    <span
                        class="big-phone__contact-dropdown-link-phone-phone fs-12 fs-lg-10 color-blue">
                                            @lang('common.main_office_phone_label_1')
                                        </span>
                </a>
            </li>

            <li class="big-phone__item-contact-entity">
                <a href="javascript:void(0)" class="big-phone__big-phone__link-contact"
                   data-bs-toggle="dropdown" aria-expanded="false">
                                        <span class="big-phone__contact-dropdown-link-phone-icon">
                                            <svg width="12" height="9" viewBox="0 0 12 9" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M6 0.25C3.91917 0.25 2.03083 1.0597 0.632917 2.37432C0.550417 2.4562 0.5 2.56992 0.5 2.69729C0.5 2.82466 0.550417 2.93838 0.632917 3.02026L1.76958 4.14838C1.85208 4.23026 1.96667 4.2803 2.10417 4.2803C2.21875 4.2803 2.33333 4.23026 2.41583 4.15293C2.77792 3.81632 3.19042 3.53429 3.635 3.31139C3.78625 3.23861 3.89167 3.08395 3.89167 2.90199V1.49184C4.55625 1.2735 5.26208 1.15977 6 1.15977C6.72875 1.15977 7.43917 1.2735 8.10375 1.48729V2.89744C8.10375 3.07485 8.20917 3.23406 8.36042 3.30684C8.80958 3.52974 9.20833 3.81632 9.58417 4.14838C9.66667 4.23026 9.78125 4.27575 9.89583 4.27575C10.0333 4.27575 10.1479 4.22571 10.2304 4.14383L11.3671 3.01571C11.4496 2.93383 11.5 2.82011 11.5 2.69274C11.5 2.56538 11.445 2.4562 11.3625 2.37432C9.96458 1.0597 8.07625 0.25 6 0.25ZM4.625 2.06955V3.43421C4.625 3.43421 1.875 5.70865 1.875 7.07331V8.89286H10.125V7.07331C10.125 5.70865 7.375 3.43421 7.375 3.43421V2.06955H6.45833V2.97932H5.54167V2.06955H4.625ZM6 4.34398C6.48623 4.34398 6.95255 4.53569 7.29636 4.87692C7.64018 5.21815 7.83333 5.68096 7.83333 6.16353C7.83333 6.64611 7.64018 7.10892 7.29636 7.45015C6.95255 7.79138 6.48623 7.98308 6 7.98308C5.51377 7.98308 5.04745 7.79138 4.70364 7.45015C4.35982 7.10892 4.16667 6.64611 4.16667 6.16353C4.16667 5.68096 4.35982 5.21815 4.70364 4.87692C5.04745 4.53569 5.51377 4.34398 6 4.34398V4.34398ZM6 5.02632C5.69611 5.02632 5.40466 5.14613 5.18977 5.3594C4.97489 5.57267 4.85417 5.86192 4.85417 6.16353C4.85417 6.46514 4.97489 6.7544 5.18977 6.96767C5.40466 7.18094 5.69611 7.30075 6 7.30075C6.30389 7.30075 6.59534 7.18094 6.81023 6.96767C7.02511 6.7544 7.14583 6.46514 7.14583 6.16353C7.14583 5.86192 7.02511 5.57267 6.81023 5.3594C6.59534 5.14613 6.30389 5.02632 6 5.02632V5.02632Z"
                                                        fill="#2C79D6"/>
                                            </svg>
                                        </span>
                    <span
                        class="big-phone__contact-dropdown-link-phone-phone fs-12 fs-lg-10 color-blue">
                                            @lang('common.main_office_phone_label_1')
                                        </span>
                </a>
            </li>
        </ul>

    </div>
</div>
</body>

</html>
