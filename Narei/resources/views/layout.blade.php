<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/style.css">
    <title>@yield('title')</title>
</head>

<body>

<div class="container">
    <header>
        <img class="logo-header" src="img/logo-header.png" width="173" height="76" align="left" alt="logo">

        <ul class="nav-header">
            <li class="nav-item-header"><a href="/members" class="nav-link">MEMBERS</a></li>
            <li class="nav-item-header"><a href="/forms" class="nav-link">FORMS</a></li>
            <li class="nav-item-header"><a href="/news&events" class="nav-link">NEWS & EVENTS</a></li>
            <li class="nav-item-header"><a href="/statistics" class="nav-link">STATISTICS</a></li>
        </ul>
    </header>

    <div class="header-strip">
        <img class="header-strip-logo" src="img/header-strip-logo.png" width="332px" height="100px" align="left"
             alt="header-strip-logo">
        <div class="header-strip-rectangle"></div>
        <h1 class="header-strip-title">MEMBER</h1>
    </div>

    @yield('main_content')

    <div>
        <footer>
            <img class="logo-footer" src="img/logo-footer.png" width="285" height="125" align="left" alt="logo">

            <ul class="nav-footer">
                <li class="nav-item-footer"><a href="/contact-us" class="nav-link">CONTACT US</a></li>
                <li class="nav-item-footer"><a href="/careers" class="nav-link">CAREERS</a></li>
                <li class="nav-item-footer"><a href="/join" class="nav-link">JOIN</a></li>
                <li class="nav-item-footer"><a href="/consumers" class="nav-link">CONSUMERS</a></li>
                <li class="nav-item-footer"><a href="/about" class="nav-link">ABOUT</a></li>
            </ul>
        </footer>
    </div>

</div>
</body>

</html>
