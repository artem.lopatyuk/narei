@extends('layout')

@section('title')

    Main Page

@endsection

@section('main_content')

    <div class="content">

        <h1>Fostering successful and long-term investments in Moldova</h1>
        <h2>The goal of the Association is to connect companies and physical persons of the<br>Real Estate Industry with
            the following targets.</h2>

        <div id="rectangle" class="member-1"></div>

        <div id="rectangle" class="member-2"></div>

        <div id="rectangle" class="member-3"></div>

        <div id="rectangle" class="member-4"></div>

            <div class="content-strip"></div>

            <h1 class="content-strip-text">Become a member of NAREI!</h1>

            <img class="content-strip-logo" src="img/img.png" width="60%" height="565px" align="right"
                 alt="content-strip-logo">
        <button class="content-strip-button">JOIN NOW</button>
    </div>

@endsection
